# -*- coding: utf-8 -*-

INSTALLED_ADDONS = [
  # <INSTALLED_ADDONS>  # Warning: text inside the INSTALLED_ADDONS tags is auto-generated. Manual changes will be overwritten.
  'aldryn-addons',
  'aldryn-django',
  'aldryn-sso',
  'aldryn-django-cms',
  'aldryn-newsblog',
  'djangocms-file',
  'djangocms-googlemap',
  'djangocms-history',
  'djangocms-link',
  'djangocms-picture',
  'djangocms-snippet',
  'djangocms-style',
  'djangocms-text-ckeditor',
  'djangocms-video',
  'django-filer',
  # </INSTALLED_ADDONS>
]

import aldryn_addons.settings

aldryn_addons.settings.load(locals())

# all django settings can be altered here

INSTALLED_APPS.extend([

  # add your project specific apps here
])

TIME_ZONE = 'Asia/Dhaka'


LANGUAGES = (
  ## Customize this
  ('en', 'English'),
  ('bn', 'Bangla'),
)

CMS_LANGUAGES = {
  ## Customize this
  1: [
    {
      'public': True,
      'code': 'en',
      'hide_untranslated': False,
      'name': 'English',
      'redirect_on_fallback': True,
    },
    {
      'public': True,
      'code': 'bn',
      'name': 'Bangla',
      'redirect_on_fallback': True,
    }
  ],

  'default': {
    'public': True,
    'hide_untranslated': False,
    'redirect_on_fallback': True,
    'fallbacks': ['en', 'bn'],
  },
}

try:
  from local_settings import *
except ImportError:
  pass
